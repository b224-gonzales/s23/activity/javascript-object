function Pokemon(name, level) {
  // Properties
  this.name = name;
  this.level = level;
  this.health = 5 * level;
  this.attack = 2 * level;

  // Methods
  this.tackle = function (target) {
    console.log(this.name + " tackled " + target.name);
    target.health = target.health - this.attack;
    if (target.health <= 0) {
      target.faint();
    } else {
      console.log(target.name + "'s health is now reduced to " + target.health);
    }
  };

  this.faint = function () {
    console.log(this.name + " fainted.");
  };
}


let bulbasaur = new Pokemon("Bulbasaur", 45);
let primeApe = new Pokemon("Prime Ape", 25);

console.log(bulbasaur);
console.log(primeApe);

bulbasaur.tackle(primeApe);
primeApe.tackle(bulbasaur);
bulbasaur.tackle(primeApe);


